class Task {
  String id;
  String name;
  String description;

  Task({required this.id, required this.name, required this.description});
}
