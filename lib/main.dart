import 'package:flutter/material.dart';
import 'package:to_do_app1/sql_helper.dart';
import 'package:to_do_app1/task.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Home(),
    );
  }
}

class Home extends StatefulWidget {
  Home({
    super.key,
  });

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final TextEditingController _textFieldController = TextEditingController();
  final TextEditingController _descriptionFieldController =
      TextEditingController();

  List<Task> task = [];
  List<Map<String, dynamic>> _task = [];
  bool _isloading = true;
  void _refreshTask() async {
    final data = await SQLHelper.getItems();
    setState(() {
      _task = data;
      _isloading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    _refreshTask();
    print(_task.length);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 232, 232, 232),
      appBar: AppBar(
        centerTitle: true,
        title: Text('Todo app'),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          openDialog(context);
        },
        child: Icon(Icons.add),
      ),
      body: ListView.builder(
        itemCount: _task.length,
        itemBuilder: (context, index) => Container(
          margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          decoration: BoxDecoration(
            color: Colors.white,
          ),
          child: ListTile(
            contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            title: Text(_task[index]['name']),
            subtitle: Text(_task[index]['description']),
            trailing: IconButton(
              onPressed: () => deleteTask(_task[index]['id']),
              icon: Icon(Icons.delete),
            ),
          ),
        ),
      ),
    );
  }

  void openDialog(BuildContext context) => showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text('Add task'),
          content: SizedBox(
            height: 100,
            child: Column(
              children: [
                TextField(
                  controller: _textFieldController,
                  decoration: InputDecoration(hintText: 'Task name'),
                ),
                TextField(
                  controller: _descriptionFieldController,
                  decoration: InputDecoration(hintText: 'Description'),
                ),
              ],
            ),
          ),
          actions: [
            TextButton(
                onPressed: () => Navigator.pop(context), child: Text('Cancel')),
            TextButton(
                onPressed: () {
                  addTask();
                  Navigator.pop(context);
                },
                child: Text('OK')),
          ],
        ),
      );

  void addTask() {
    // print(_textFieldController.text);
    setState(() {
      // task.add(Task(
      //     id: DateTime.now().millisecondsSinceEpoch.toString(),
      //     name: _textFieldController.text,
      //     description: _descriptionFieldController.text));
      SQLHelper.createItem(_textFieldController.text.toString(),
          _descriptionFieldController.text.toString());
      _textFieldController.clear();
      _descriptionFieldController.clear();
      _refreshTask();
      print("number task ${_task.length}");
    });
  }

  void deleteTask(int id) {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text('Delete Task'),
        actions: [
          TextButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text('Cancel')),
          TextButton(
              onPressed: () {
                setState(() {
                  // task.removeWhere((element) => element.id == id);
                  SQLHelper.deleteItem(id);
                  _refreshTask();
                });
                Navigator.pop(context);
              },
              child: Text('Ok')),
        ],
      ),
    );
  }
}
